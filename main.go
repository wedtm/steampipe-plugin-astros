package main

import (
	"steampipe-plugin-astros/astros"

	"github.com/turbot/steampipe-plugin-sdk/plugin"
)

func main() {
	plugin.Serve(&plugin.ServeOpts{PluginFunc: astros.Plugin})
}
