package astros

type AstrosResponse struct {
	People []struct {
		Name  string `json:"name"`
		Craft string `json:"craft"`
	} `json:"people"`
	Number  int    `json:"number"`
	Message string `json:"message"`
}
