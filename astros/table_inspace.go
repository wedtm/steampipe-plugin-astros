package astros

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/turbot/steampipe-plugin-sdk/grpc/proto"
	"github.com/turbot/steampipe-plugin-sdk/plugin"
)

func tableInSpace() *plugin.Table {
	return &plugin.Table{
		Name:        "astros_inspace",
		Description: "Astronauts in space",
		List: &plugin.ListConfig{
			Hydrate: listAstros,
		},
		Get: &plugin.GetConfig{
			KeyColumns: plugin.SingleColumn("id"),
			Hydrate:    listAstros,
		},
		Columns: []*plugin.Column{
			{Name: "name", Type: proto.ColumnType_STRING, Description: "Astronauts name"},
			{Name: "craft", Type: proto.ColumnType_STRING, Description: "Name of the craft"},
		},
	}
}

func listAstros(ctx context.Context, d *plugin.QueryData, _ *plugin.HydrateData) (interface{}, error) {
	response, err := http.Get("http://api.open-notify.org/astros.json")

	if err != nil {
		return nil, err
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	var respObj AstrosResponse
	json.Unmarshal(responseData, &respObj)

	for _, t := range respObj.People {
		d.StreamListItem(ctx, t)
	}

	return nil, nil
}
